use std::collections::HashSet;
use std::ops::Add;
use itertools::Itertools;
use crate::DaySolver;

pub struct Day06;

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
struct Coordinate {
    pub x: i32,
    pub y: i32
}

impl Add for &Coordinate {
    type Output = Coordinate;

    fn add(self, rhs: Self) -> Self::Output {
        Coordinate {
            x: self.x + rhs.x,
            y: self.y + rhs.y
        }
    }
}

const DIRECTIONS: [Coordinate; 4] = [
    Coordinate { x: 0, y: -1 },
    Coordinate { x: 1, y: 0},
    Coordinate { x: 0, y: 1},
    Coordinate { x: -1, y: 0}
];

impl DaySolver for Day06 {
    fn solve_part1(&self, input: &str) -> i32 {
        let mut pos = input.lines().enumerate()
            .find_map(|(y, l)|
                l.find("^")
                    .map(|x| Coordinate {
                        x: x as i32,
                        y: y as i32
                    })
            ).unwrap();

        let obstacles = input.lines().enumerate().flat_map(move |(y, l)| {
            l.chars().enumerate().filter_map(move |(x, c)| if c == '#' { Some(Coordinate {
                x: x as i32,
                y: y as i32
            })} else { None })
        }).collect::<HashSet<Coordinate>>();

        let max_y = (input.lines().collect_vec().len() - 1) as i32;
        let max_x = (input.lines().next().unwrap().len() - 1) as i32;
        let mut dir_index = 0;
        let mut visited = HashSet::new();

        while pos.x >= 0 && pos.y >= 0 && pos.x <= max_x && pos.y <= max_y {
            visited.insert(pos.clone());

            if obstacles.contains(&(&pos + &DIRECTIONS[dir_index])) {
                dir_index = (dir_index + 1) % 4;
            }

            pos = &pos + &DIRECTIONS[dir_index]
        }

        visited.len() as i32
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let starting_pos = input.lines().enumerate()
            .find_map(|(y, l)|
            l.find("^")
                .map(|x| Coordinate {
                    x: x as i32,
                    y: y as i32
                })
            ).unwrap();

        let existing_obstacles = input.lines().enumerate().flat_map(move |(y, l)| {
            l.chars().enumerate().filter_map(move |(x, c)| if c == '#' { Some(Coordinate {
                x: x as i32,
                y: y as i32
            })} else { None })
        }).collect::<HashSet<Coordinate>>();

        let potential_obstacles = input.lines().enumerate().flat_map(move |(y, l)| {
            l.chars().enumerate().filter_map(move |(x, c)| if c == '.' { Some(Coordinate {
                x: x as i32,
                y: y as i32
            })} else { None })
        }).collect::<HashSet<Coordinate>>();

        let max_y = (input.lines().collect_vec().len() - 1) as i32;
        let max_x = (input.lines().next().unwrap().len() - 1) as i32;

        println!("Trying {} obstacles", potential_obstacles.len());

        potential_obstacles.into_iter().enumerate().filter(|(i, obst)| {
            if i % 1000 == 0 {
                println!("Trying number {}", i)
            }
            let mut obstacles = existing_obstacles.clone();
            obstacles.insert(obst.clone());
            let mut dir_index = 0;
            let mut visited = HashSet::new();
            let mut pos = starting_pos.clone();

            while pos.x >= 0 && pos.y >= 0 && pos.x <= max_x && pos.y <= max_y {
                if visited.contains(&(pos.clone(), dir_index)) {
                    return true
                }

                visited.insert((pos.clone(), dir_index));

                if obstacles.contains(&(&pos + &DIRECTIONS[dir_index])) {
                    dir_index = (dir_index + 1) % 4;
                } else {
                    pos = &pos + &DIRECTIONS[dir_index]
                }
            }

            false
        }).collect_vec().len() as i32
    }
}
