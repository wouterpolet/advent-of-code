use std::ops::{Add, Sub};

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
pub struct Coordinate {
    pub x: i32,
    pub y: i32
}

impl Add for &Coordinate {
    type Output = Coordinate;

    fn add(self, rhs: Self) -> Self::Output {
        Coordinate {
            x: self.x + rhs.x,
            y: self.y + rhs.y
        }
    }
}

impl Sub for &Coordinate {
    type Output = Coordinate;

    fn sub(self, rhs: Self) -> Self::Output {
        Coordinate {
            x: self.x - rhs.x,
            y: self.y - rhs.y
        }
    }
}