use std::collections::{HashMap, HashSet, VecDeque};
use itertools::Itertools;
use crate::DaySolver;

pub struct Day12;

impl DaySolver for Day12 {
    fn solve_part1(&self, input: &str) -> i32 {
        let map = input.lines().map(|l| l.chars().collect_vec()).collect_vec();

        let mut un_assigned = (0usize..map.len()).flat_map(|y| {
            (0usize..map[y].len()).map(move |x| { (y, x) })
        }).collect_vec();

        let max_x = map.len() - 1;
        let max_y = map[0].len() - 1;
        let mut areas = Vec::<(char, HashSet<(usize, usize)>)>::new();
        let mut perimeters = HashMap::<(usize, usize), i32>::new();

        un_assigned.clone().into_iter().for_each(|(x, y)| {
            let mut perimeter = 0;
            let crop = map[y][x];

            if x == 0 || map[y][x-1] != crop {
                perimeter += 1;
            }
            if y == 0 || map[y-1][x] != crop {
                perimeter += 1;
            }
            if x == max_x || map[y][x+1] != crop {
                perimeter += 1;
            }
            if y == max_y || map[y+1][x] != crop {
                perimeter += 1;
            }

            perimeters.insert((x, y), perimeter);
        });

        while let Some(start) = un_assigned.pop() {
            let mut visited = HashSet::new();
            let mut queue = VecDeque::new();
            queue.push_back(start);

            while let Some(current@(x, y)) = queue.pop_front() {
                if visited.contains(&current) {
                    continue;
                }

                visited.insert(current);
                if let Some((pos, _)) = un_assigned.clone().into_iter().find_position(|c| *c == current) {
                    un_assigned.remove(pos);
                }

                let crop = map[y][x];

                if x > 0 && map[y][x-1] == crop && !visited.contains(&(x-1, y)) && !queue.contains(&(x-1, y)) {
                    queue.push_back((x-1, y));
                }
                if y > 0 && map[y-1][x] == crop && !visited.contains(&(x, y-1)) && !queue.contains(&(x, y-1)) {
                    queue.push_back((x, y-1));
                }
                if x < max_x && map[y][x+1] == crop && !visited.contains(&(x+1, y)) && !queue.contains(&(x+1, y)) {
                    queue.push_back((x+1, y));
                }
                if y < max_y && map[y+1][x] == crop && !visited.contains(&(x, y+1)) && !queue.contains(&(x, y+1)) {
                    queue.push_back((x, y+1));
                }
            }

            areas.push((map[start.1][start.0], visited));
        }

        // println!("{:?}", areas);

        areas.into_iter().map(|(_, fields)| {
            fields.len() as i32 * fields.clone().into_iter().map(|(x, y)| perimeters.get(&(x, y)).unwrap()).sum::<i32>()
        }).sum()
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let map = input.lines().map(|l| l.chars().collect_vec()).collect_vec();

        let mut un_assigned = (0usize..map.len()).flat_map(|y| {
            (0usize..map[y].len()).map(move |x| { (y, x) })
        }).collect_vec();

        let max_x = map.len() - 1;
        let max_y = map[0].len() - 1;
        let mut areas = Vec::<(char, HashSet<(usize, usize)>)>::new();

        while let Some(start) = un_assigned.pop() {
            let mut visited = HashSet::new();
            let mut queue = VecDeque::new();
            queue.push_back(start);

            while let Some(current@(x, y)) = queue.pop_front() {
                if visited.contains(&current) {
                    continue;
                }

                visited.insert(current);
                if let Some((pos, _)) = un_assigned.clone().into_iter().find_position(|c| *c == current) {
                    un_assigned.remove(pos);
                }

                let crop = map[y][x];

                if x > 0 && map[y][x-1] == crop && !visited.contains(&(x-1, y)) && !queue.contains(&(x-1, y)) {
                    queue.push_back((x-1, y));
                }
                if y > 0 && map[y-1][x] == crop && !visited.contains(&(x, y-1)) && !queue.contains(&(x, y-1)) {
                    queue.push_back((x, y-1));
                }
                if x < max_x && map[y][x+1] == crop && !visited.contains(&(x+1, y)) && !queue.contains(&(x+1, y)) {
                    queue.push_back((x+1, y));
                }
                if y < max_y && map[y+1][x] == crop && !visited.contains(&(x, y+1)) && !queue.contains(&(x, y+1)) {
                    queue.push_back((x, y+1));
                }
            }

            areas.push((map[start.1][start.0], visited));
        }

        // println!("{:?}", areas);

        areas.into_iter().map(|(_, fields)| {
            let mut sides = 0;

            fields.clone().into_iter().for_each(|(x, y)| {
                let crop = map[y][x];

                if (x == 0 || map[y][x-1] != crop) && (y == 0 || map[y-1][x] != crop || (x != 0 && map[y-1][x-1] == crop)) {
                    sides += 1;
                }
                if (x == max_x || map[y][x+1] != crop) && (y == 0 || map[y-1][x] != crop || (x != max_x && map[y-1][x+1] == crop)) {
                    sides += 1;
                }
                if (y == 0 || map[y-1][x] != crop) && (x == 0 || map[y][x-1] != crop || (y != 0 && map[y-1][x-1] == crop)) {
                    sides += 1;
                }
                if (y == max_y || map[y+1][x] != crop) && (x == 0 || map[y][x-1] != crop || (y != max_y && map[y+1][x-1] == crop)) {
                    sides += 1;
                }
            });

            fields.len() as i32 * sides
        }).sum()
    }
}
