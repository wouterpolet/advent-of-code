use itertools::Itertools;
use crate::DaySolver;

pub struct Day04;

const DIRECTIONS: &[(i32, i32); 8] = &[
    (0, 1),
    (0, -1),
    (1, 0),
    (-1, 0),
    (1, 1),
    (-1, 1),
    (1, -1),
    (-1, -1)
];

const DIAGONALS: &[(i32, i32); 4] = &[
    (-1, -1),
    (1, 1),
    (1, -1),
    (-1, 1)
];

const WORD: &[u8] = "XMAS".as_bytes();

impl DaySolver for Day04 {
    fn solve_part1(&self, input: &str) -> i32 {
        let chars: Vec<Vec<&u8>> = input.lines().map(|l| l.as_bytes().iter().collect_vec()).collect_vec();

        let mut sum = 0;
        for i in 0..chars.len() as i32 {
            for j in 0..chars[i as usize].len() as i32 {
                for (dir_i, dir_j) in DIRECTIONS {
                    for k in 0..WORD.len() as i32 {
                        let current_i = i + *dir_i * k;
                        let current_j = j + *dir_j * k;

                        if current_i < 0
                            || current_j < 0
                            || current_i >= chars.len() as i32
                            || current_j >= chars[current_i as usize].len() as i32
                            || *chars[current_i as usize][current_j as usize] != WORD[k as usize] {

                            break;
                        } else if k == WORD.len() as i32 - 1 {
                            sum += 1;
                        }
                    }
                }
            }
        }

        sum
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let chars: Vec<Vec<char>> = input.lines().map(|l| l.chars().collect_vec()).collect_vec();

        let mut sum = 0;
        for i in 1..chars.len() - 1 {
            for j in 1..chars[i].len() - 1 {
                let diagonal_chars: Vec<char> = DIAGONALS.map(|(dir_i, dir_j)| {
                    chars[(i as i32 + dir_i) as usize][(j as i32 + dir_j) as usize].clone()
                }).into_iter().collect_vec();

                let counts = diagonal_chars.clone().into_iter().counts();

                if chars[i][j] == 'A'
                    && counts.get(&'M') == Some(&2)
                    && counts.get(&'S') == Some(&2)
                    && diagonal_chars[0] != diagonal_chars[1]
                    && diagonal_chars[2] != diagonal_chars[3] {

                    sum += 1
                }
            }
        }

        sum
    }
}
