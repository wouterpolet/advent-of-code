use regex::Regex;
use crate::DaySolver;

pub struct Day03;

impl DaySolver for Day03 {

    fn solve_part1(&self, input: &str) -> i32 {
        let mul_regex: Regex = Regex::new(r"mul\((\d+),(\d+)\)").unwrap();

        mul_regex.captures_iter(input)
            .map(|caps| &caps[1].parse::<i32>().unwrap() * &caps[2].parse::<i32>().unwrap())
            .sum()
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let ops = Regex::new(r"(mul|do|don't)\((?:(\d+),(\d+))?\)").unwrap();

        let mut enabled = true;
        let mut sum = 0;
        ops.captures_iter(input)
            .for_each(|caps| {
                match &caps[1] {
                    "mul" if enabled => { sum += &caps[2].parse::<i32>().unwrap() * &caps[3].parse::<i32>().unwrap()}
                    "do" => { enabled = true }
                    "don't" => { enabled = false }
                    _ => {}
                }
            });

        sum
    }
}
