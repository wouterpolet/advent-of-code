use itertools::Itertools;
use crate::DaySolver;

pub struct Day02;

fn parse(input: &str) -> Vec<Vec<i32>> {
    input.lines()
        .map(|l|
            l.split_ascii_whitespace()
            .map(|c| c.parse::<i32>().unwrap()).collect_vec())
        .collect_vec()
}

impl DaySolver for Day02 {
    fn solve_part1(&self, input: &str) -> i32 {
        let reports = parse(input);

        reports.iter()
            .filter(|r| r.is_sorted() || r.iter().rev().is_sorted())
            .filter(|r| r.windows(2).all(|w| {
                let diff = (w[0] - w[1]).abs();
                return diff >= 1 && diff <= 3
            }))
            .count() as i32
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let reports = parse(input);

        reports.iter()
            .filter(|r| (0..r.len()).any(|i| {
                let mut new_r = (*r).clone();
                new_r.remove(i);

                (new_r.is_sorted() || new_r.iter().rev().is_sorted()) &&
                    new_r.windows(2).all(|w| {
                        let diff = (w[0] - w[1]).abs();
                        return diff >= 1 && diff <= 3
            })}))
            .count() as i32
    }
}
