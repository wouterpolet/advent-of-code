use std::collections::HashSet;
use itertools::Itertools;
use crate::DaySolver;

pub struct Day07;

impl DaySolver for Day07 {
    fn solve_part1_i64(&self, input: &str) -> i64 {
        let equations: Vec<(i64, Vec<i64>)> = input.lines()
            .map(|l| l.split_once(": ").unwrap())
            .map(|(test, subject)| (
                test.parse::<i64>().unwrap(),
                subject.split_ascii_whitespace().map(|num| num.parse::<i64>().unwrap()).collect_vec()
            )).collect_vec();

        equations.iter().filter(|(test, nums)| {
            nums.iter().fold(HashSet::new(), |acc, num| {
                if acc.is_empty() {
                    let mut res = HashSet::new();
                    res.insert(num.clone());
                    res
                } else {
                    acc.into_iter().flat_map(|acc_num| {
                        [acc_num + num, acc_num * num].into_iter()
                    }).collect::<HashSet<i64>>()
                }
            }).contains(test)
        }).map(|(test, _)| test).sum()
    }

    fn solve_part2_i64(&self, input: &str) -> i64 {
        let equations: Vec<(i64, Vec<i64>)> = input.lines()
            .map(|l| l.split_once(": ").unwrap())
            .map(|(test, subject)| (
                test.parse::<i64>().unwrap(),
                subject.split_ascii_whitespace().map(|num| num.parse::<i64>().unwrap()).collect_vec()
            )).collect_vec();

        equations.iter().filter(|(test, nums)| {
            nums.iter().fold(HashSet::new(), |acc, num| {
                if acc.is_empty() {
                    let mut res = HashSet::new();
                    res.insert(num.clone());
                    res
                } else {
                    acc.into_iter().flat_map(|acc_num| {
                        let mut options = Vec::new();
                        if acc_num + num <= test.clone() {
                            options.insert(0, acc_num + num);
                        }
                        if acc_num * num <= test.clone() {
                            options.insert(0, acc_num * num)
                        }
                        if acc_num.ilog10() + num.ilog10() <= test.ilog10() {
                            options.insert(0, (acc_num.to_string() + &*num.to_string()).parse::<i64>().unwrap());
                        }
                        options
                    }).collect::<HashSet<i64>>()
                }
            }).contains(test)
        }).map(|(test, _)| test).sum()
    }
}
