use std::cmp::Ordering;
use std::collections::HashSet;
use itertools::Itertools;
use crate::DaySolver;

pub struct Day05;

impl DaySolver for Day05 {
    fn solve_part1(&self, input: &str) -> i32 {
        let (rules_str, prints_str) = input.split_once("\n\n").unwrap();

        let rules = rules_str.lines()
            .map(|l| l.split_once("|").unwrap())
            .map(|(l, r)| (l.parse::<i32>().unwrap(), r.parse::<i32>().unwrap()))
            .into_grouping_map_by(|(l, _)| *l)
            .fold(HashSet::new(), |mut acc, _key, (_, r)| {
                acc.insert(r);
                acc
            });
        let prints = prints_str.lines()
            .map(|l| l.split(",").map(|c| c.parse::<i32>().unwrap()).collect_vec())
            .collect_vec();

        prints.iter().filter(|print| {
            let mut acc = HashSet::<i32>::new();
            let mut valid = true;

            for page in print.iter() {
                if !acc.is_disjoint(rules.get(page).unwrap_or(&HashSet::new())) {
                    valid = false;
                    break;
                }
                acc.insert(*page);
            }

            valid
        }).map(|page| {
            page.get(page.len() / 2).unwrap()
        }).sum()
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let (rules_str, prints_str) = input.split_once("\n\n").unwrap();

        let rules = rules_str.lines()
            .map(|l| l.split_once("|").unwrap())
            .map(|(l, r)| (l.parse::<i32>().unwrap(), r.parse::<i32>().unwrap()))
            .into_grouping_map_by(|(l, _)| *l)
            .fold(HashSet::new(), |mut acc, _key, (_, r)| {
                acc.insert(r);
                acc
            });
        let prints = prints_str.lines()
            .map(|l| l.split(",").map(|c| c.parse::<i32>().unwrap()).collect_vec())
            .collect_vec();

        prints.iter().filter(|print| {
            let mut acc = HashSet::<i32>::new();

            for page in print.iter() {
                if !acc.is_disjoint(rules.get(page).unwrap_or(&HashSet::new())) {
                    return true;
                }
                acc.insert(*page);
            }

            false
        }).map(|page| {
            *page.iter().sorted_by(|l, r| if rules.contains_key(l) && rules.get(l).unwrap().contains(r) {
                Ordering::Less
            } else if rules.contains_key(r) && rules.get(r).unwrap().contains(l) {
                Ordering::Greater
            } else {
                Ordering::Equal
            }).collect_vec().get(page.len() / 2).unwrap()
        }).sum()
    }
}
