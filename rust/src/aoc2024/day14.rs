use std::collections::HashSet;
use itertools::Itertools;
use regex::Regex;
use crate::DaySolver;

pub struct Day14;

impl DaySolver for Day14 {
    fn solve_part1(&self, input: &str) -> i32 {
        let regex = Regex::new(r"p=(\d+),(\d+) v=(-?\d+),(-?\d+)").unwrap();
        let robots: Vec<((i32, i32), (i32, i32))> = regex.captures_iter(input).map(|capt| {
            ((capt[1].parse().unwrap(), capt[2].parse().unwrap()), (capt[3].parse().unwrap(), capt[4].parse().unwrap()))
        }).collect_vec();

        let width = 101;
        let height = 103;
        let num_seconds = 100;

        let locations = robots.into_iter().map(|((start_x, start_y), (vel_x, vel_y))| {
            (((start_x + num_seconds * vel_x) % width + width) % width, ((start_y + num_seconds * vel_y) % height + height) % height)
        }).collect_vec();

        // println!("{:?}", locations);

        let mut left_top = 0;
        let mut right_top = 0;
        let mut left_bottom = 0;
        let mut right_bottom = 0;

        locations.into_iter().for_each(|(x, y)| {
            if x < width / 2 && y < height / 2 {
                left_top += 1;
            }
            if x > width / 2 && y > height / 2 {
                right_bottom += 1;
            }
            if x < width / 2 && y > height / 2 {
                left_bottom += 1;
            }
            if x > width / 2 && y < height / 2 {
                right_top += 1;
            }
        });

        // println!("LT: {left_top}, RT: {right_top}, LB: {left_bottom}, RB: {right_bottom}");

        left_top * right_top * left_bottom * right_bottom
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let regex = Regex::new(r"p=(\d+),(\d+) v=(-?\d+),(-?\d+)").unwrap();
        let robots: Vec<((i32, i32), (i32, i32))> = regex.captures_iter(input).map(|capt| {
            ((capt[1].parse().unwrap(), capt[2].parse().unwrap()), (capt[3].parse().unwrap(), capt[4].parse().unwrap()))
        }).collect_vec();

        let width = 101;
        let height = 103;
        let num_seconds = 10000;

        for i in 1..num_seconds+1 {
            if (i + 1) % 1000 == 0 {
                println!("Computing second {}", i+1);
            }

            let mut potential = false;

            let locations: HashSet<(i32, i32)> = robots.iter().map(|((start_x, start_y), (vel_x, vel_y))| {
                (((start_x + i * vel_x) % width + width) % width, ((start_y + i * vel_y) % height + height) % height)
            }).collect();

            for (x, y) in locations.clone() {
                if locations.contains(&(x, y + 1))
                    && locations.contains(&(x, y + 2))
                    && locations.contains(&(x, y + 3))
                    && locations.contains(&(x, y + 4))
                    && locations.contains(&(x, y + 5)){

                    potential = true;
                }
            }

            if potential {
                let mut image = "".to_string();

                for y in 0..101 {
                    for x in 0..103 {
                        image += if locations.contains(&(x, y)) { "█" } else { " " };
                    }
                    image += "\n"
                }

                println!("After {i} seconds:\n{image}");
            }
        }

        -1
    }
}
