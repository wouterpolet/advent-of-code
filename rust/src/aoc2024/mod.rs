use crate::{DaySolver, YearSolver};

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;

pub struct Year2024;

impl YearSolver for Year2024 {
    fn get_day_solver(&self, day: &i32) -> Option<&dyn DaySolver> {
        match day {
            1 => Some(&day01::Day01),
            2 => Some(&day02::Day02),
            3 => Some(&day03::Day03),
            4 => Some(&day04::Day04),
            5 => Some(&day05::Day05),
            6 => Some(&day06::Day06),
            7 => Some(&day07::Day07),
            8 => Some(&day08::Day08),
            9 => Some(&day09::Day09),
            10 => Some(&day10::Day10),
            11 => Some(&day11::Day11),
            12 => Some(&day12::Day12),
            13 => Some(&day13::Day13),
            14 => Some(&day14::Day14),
            15 => Some(&day15::Day15),
            _ => None
        }
    }
}
