use itertools::Itertools;
use regex::Regex;
use ndarray::prelude::*;
use ndarray_linalg::Solve;
use crate::DaySolver;

pub struct Day13;

impl DaySolver for Day13 {
    fn solve_part1(&self, input: &str) -> i32 {
        let regex = Regex::new(r"Button A: X\+(\d+), Y\+(\d+)\nButton B: X\+(\d+), Y\+(\d+)\nPrize: X=(\d+), Y=(\d+)").unwrap();
        let games: Vec<((i32, i32), (i32, i32), (i32, i32))> = regex.captures_iter(input).map(|capt| {
            (
                (capt[1].parse().unwrap(), capt[2].parse().unwrap()),
                (capt[3].parse().unwrap(), capt[4].parse().unwrap()),
                (capt[5].parse().unwrap(), capt[6].parse().unwrap())
            )
        }).collect_vec();

        games.into_iter().map(|((a_x, a_y), (b_x, b_y), (target_x, target_y))| {
            let mut min_tokens = 0;

            for num_a in 1..101 {
                for num_b in 1..101 {
                    let tokens = num_a * 3 + num_b;

                    if num_a * a_x + num_b * b_x == target_x
                        && num_a * a_y + num_b * b_y == target_y
                        && (min_tokens == 0 || tokens < min_tokens) {
                            min_tokens = tokens;
                    }
                }
            }

            min_tokens
        }).sum()
    }

    fn solve_part2_i64(&self, input: &str) -> i64 {
        let regex = Regex::new(r"Button A: X\+(\d+), Y\+(\d+)\nButton B: X\+(\d+), Y\+(\d+)\nPrize: X=(\d+), Y=(\d+)").unwrap();
        let games: Vec<((i64, i64), (i64, i64), (i64, i64))> = regex.captures_iter(input).map(|capt| {
            (
                (capt[1].parse().unwrap(), capt[2].parse().unwrap()),
                (capt[3].parse().unwrap(), capt[4].parse().unwrap()),
                (capt[5].parse::<i64>().unwrap() + 10000000000000i64, capt[6].parse::<i64>().unwrap() + 10000000000000i64)
            )
        }).collect_vec();

        games.into_iter().map(|((a_x, a_y), (b_x, b_y), (target_x, target_y))| {
            let a: Array2<f64> = array![[a_x as f64, b_x as f64], [a_y as f64, b_y as f64]];
            let b: Array1<f64> = array![target_x as f64, target_y as f64];
            let x = a.solve_into(b).unwrap();

            let solve_a = x[0].round() as i64;
            let solve_b = x[1].round() as i64;

            if solve_a * a_x + solve_b * b_x == target_x && solve_a * a_y + solve_b * b_y == target_y {
                x[0].round() as i64 * 3 + x[1].round() as i64
            } else {
                0
            }
        }).sum()
    }
}
