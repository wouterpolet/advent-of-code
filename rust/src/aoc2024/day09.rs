use itertools::Itertools;
use crate::DaySolver;

pub struct Day09;

fn get_current_length(system: &Vec<Option<i64>>, index: usize) -> usize {
    let mut size = 0;
    let mut c_index = index;
    let value = system[index];

    while system[c_index] == value {
        size += 1;

        if c_index == 0 {
            break;
        }

        c_index -= 1;
    }

    size
}

fn get_open_space_with_length(system: &Vec<Option<i64>>, size: usize) -> Option<usize> {
    let mut index = 0;
    let mut c_index = index;
    let mut found_size = 0;

    while index < system.len() {
        while c_index < system.len() && system[c_index].is_none() {
            found_size += 1;
            c_index += 1;

            if found_size >= size {
                return Some(index);
            }
        }

        c_index += 1;
        index = c_index;
        found_size = 0;
    }

    None
}

fn move_n_blocks(system: &mut Vec<Option<i64>>, open_space_index: usize, block_index: usize, size: usize) {
    for i in 0..size {
        system[open_space_index + i] = system[block_index - i];
        system[block_index - i] = None;
    }
}

impl DaySolver for Day09 {
    fn solve_part1_i64(&self, input: &str) -> i64 {
        let blocks = input.chars().map(|c| c.to_string().parse::<usize>().unwrap()).collect_vec();
        let size = blocks.iter().sum();
        let mut system = Vec::with_capacity(size);
        let mut file_index = 0;
        let mut file = true;

        for block in blocks {
            for _  in 0..block {
                if file {
                    system.push(Some(file_index));
                } else {
                    system.push(None);
                }
            }
            if file {
                file_index += 1;
            }
            file = !file;
        }

        let mut open_space_index = system.iter().find_position(|item| item.is_none()).unwrap().0;
        let mut current_move_index = system.iter().enumerate().rfind(|(_, item)| item.is_some()).unwrap().0;

        loop {
            system[open_space_index] = system[current_move_index];
            system[current_move_index] = None;

            while open_space_index < system.len() && system[open_space_index].is_some() {
                open_space_index += 1;
            }

            while current_move_index >= 1 && system[current_move_index].is_none() {
                current_move_index -= 1;
            }

            if open_space_index >= system.len() || current_move_index == 0 || open_space_index >= current_move_index {
                break;
            }
        }

        system.into_iter().enumerate().filter(|(_, item)| item.is_some()).map(|(pos, item)| pos as i64 * item.unwrap()).sum()
    }

    fn solve_part2_i64(&self, input: &str) -> i64 {
        let blocks = input.chars().map(|c| c.to_string().parse::<usize>().unwrap()).collect_vec();
        let size = blocks.iter().sum();
        let mut system = Vec::with_capacity(size);
        let mut file_index = 0;
        let mut file = true;

        for block in blocks {
            for _  in 0..block {
                if file {
                    system.push(Some(file_index));
                } else {
                    system.push(None);
                }
            }
            if file {
                file_index += 1;
            }
            file = !file;
        }

        // println!("before {}", system.iter().map(|item| if item.is_none() { ".".to_string() } else { item.unwrap().to_string() }).join(""));

        let mut current_move_index = system.iter().enumerate().rfind(|(_, item)| item.is_some()).unwrap().0;
        let mut current_size = get_current_length(&system, current_move_index);
        let mut current_open_space = get_open_space_with_length(&system, current_size).unwrap(); // First one has to be able to move

        loop {
            // println!("Moving {} blocks from {} to {}", current_size, current_move_index, current_open_space);

            move_n_blocks(&mut system, current_open_space, current_move_index, current_size);

            // println!("update {}", system.iter().map(|item| if item.is_none() { ".".to_string() } else { item.unwrap().to_string() }).join(""));

            loop {
                while current_move_index >= 1 && system[current_move_index].is_none() {
                    current_move_index -= 1;
                }
                current_size = get_current_length(&system, current_move_index);
                let next_open_space = get_open_space_with_length(&system, current_size);

                if let Some(index) = next_open_space {
                    current_open_space = index;
                    if current_open_space < current_move_index {
                        break;
                    }
                }

                if current_move_index < current_size {
                    break;
                }

                current_move_index -= current_size;
            }

            if current_move_index < current_size {
                break;
            }
        }

        // println!("after  {}", system.iter().map(|item| if item.is_none() { ".".to_string() } else { item.unwrap().to_string() }).join(""));

        system.into_iter().enumerate().filter(|(_, item)| item.is_some()).map(|(pos, item)| pos as i64 * item.unwrap()).sum()
    }
}
