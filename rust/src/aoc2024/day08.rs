use std::collections::{HashMap, HashSet};
use itertools::Itertools;
use crate::DaySolver;
use crate::util::Coordinate;

pub struct Day08;

impl DaySolver for Day08 {
    fn solve_part1(&self, input: &str) -> i32 {
        let antennas: HashMap<char, Vec<Coordinate>> = input.lines().enumerate().flat_map(move |(y, l)| {
            l.chars().enumerate().filter_map(move |(x, c)| if c != '.' { Some((c, Coordinate {
                x: x as i32,
                y: y as i32
            }))} else { None })
        }).into_group_map();

        let mut antinodes = HashSet::new();
        let lines = input.lines().collect_vec();
        let max_y = lines.len() as i32;
        let max_x = lines[0].len() as i32;
        let min_coor = Coordinate {
            x: 0,
            y: 0
        };
        let max_coor = Coordinate {
            x: max_x - 1,
            y: max_y - 1
        };

        for antenna in antennas.values() {
            for a in antenna {
                for b in antenna {
                    if a == b {
                        continue;
                    }

                    let antinode = a + &(a - b);

                    if antinode.x >= min_coor.x && antinode.y >= min_coor.y && antinode.x <= max_coor.x && antinode.y <= max_coor.y {
                        antinodes.insert(antinode);
                    }
                }
            }
        }

        antinodes.len() as i32
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let antennas: HashMap<char, Vec<Coordinate>> = input.lines().enumerate().flat_map(move |(y, l)| {
            l.chars().enumerate().filter_map(move |(x, c)| if c != '.' { Some((c, Coordinate {
                x: x as i32,
                y: y as i32
            }))} else { None })
        }).into_group_map();

        let mut antinodes = HashSet::new();
        let lines = input.lines().collect_vec();
        let max_y = lines.len() as i32 - 1;
        let max_x = lines[0].len() as i32 - 1;

        for antenna in antennas.values() {
            for a in antenna {
                for b in antenna {
                    if a == b {
                        continue;
                    }

                    let diff = &(a - b);
                    let mut current = a.clone();

                    while current.x >= 0 && current.y >= 0 && current.x <= max_x && current.y <= max_y {
                        antinodes.insert(current);
                        current = &current + diff;
                    }
                }
            }
        }

        antinodes.len() as i32
    }
}
