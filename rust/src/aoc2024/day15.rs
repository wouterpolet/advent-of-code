use std::collections::{HashSet, VecDeque};
use crate::DaySolver;
use crate::util::Coordinate;

pub struct Day15;

fn print_maze(walls: &HashSet<Coordinate>, boxes: &HashSet<Coordinate>, robot: &Coordinate) {
    let mut maze = "".to_string();

    for y in 0..walls.iter().map(|Coordinate { x: _, y }| y).max().unwrap() + 1 {
        for x in 0..walls.iter().map(|Coordinate { x, y: _ }| x).max().unwrap() + 1 {
            let location = Coordinate { x, y };

            if walls.contains(&location) {
                maze += "#";
            } else if boxes.contains(&location) {
                maze += "[";
            } else if boxes.contains(&(&location + &Coordinate { x: -1, y: 0 })) {
                maze += "]";
            } else if robot == &location {
                maze += "@";
            } else {
                maze += ".";
            }
        }
        maze += "\n";
    }

    println!("{maze}");
}

impl DaySolver for Day15 {
    fn solve_part1(&self, input: &str) -> i32 {
        let (maze, instructions) = input.split_once("\n\n").map(|(m, instr)| {
            (m, instr.replace("\n", ""))
        }).unwrap();

        let mut walls = HashSet::new();
        let mut boxes = HashSet::new();
        let mut robot = Coordinate { x: -1, y: -1 };

        maze.lines().enumerate().for_each(|(y, l)| {
            l.chars().enumerate().for_each(|(x, c)| {
                match c {
                    '#' => {
                        walls.insert(Coordinate { x: x as i32, y: y as i32 });
                    }
                    'O' => {
                        boxes.insert(Coordinate { x: x as i32, y: y as i32 });
                    }
                    '@' => {
                        robot = Coordinate { x: x as i32, y: y as i32 };
                    }
                    _ => {}
                }
            });
        });

        instructions.chars().for_each(|instr| {
            let direction = match instr {
                '^' => Coordinate { x: 0, y: -1 },
                '>' => Coordinate { x: 1, y: 0 },
                '<' => Coordinate { x: -1, y: 0 },
                'v' => Coordinate { x: 0, y: 1 },
                _ => panic!("Invalid instruction: {instr}")
            };
            let new_loc = &robot + &direction;

            if boxes.contains(&new_loc) {
                let mut after_box = new_loc;

                while boxes.contains(&after_box) {
                    after_box = &after_box + &direction;
                }

                if !walls.contains(&after_box) {
                    boxes.remove(&new_loc);
                    boxes.insert(after_box);

                    robot = new_loc;
                }
            } else if !walls.contains(&new_loc) {
                robot = new_loc;
            }

            // println!("Robot after {instr}: {:?}", robot);
        });

        boxes.iter().map(|Coordinate { x, y }| x + y * 100).sum()
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let (maze, instructions) = input.split_once("\n\n").map(|(m, instr)| {
            (m, instr.replace("\n", ""))
        }).unwrap();

        let mut walls = HashSet::new();
        let mut boxes = HashSet::new();
        let mut robot = Coordinate { x: -1, y: -1 };

        maze.lines().enumerate().for_each(|(y, l)| {
            l.chars().enumerate().for_each(|(x, c)| {
                match c {
                    '#' => {
                        walls.insert(Coordinate { x: (x * 2) as i32, y: y as i32 });
                        walls.insert(Coordinate { x: (x * 2 + 1) as i32, y: y as i32 });
                    }
                    'O' => {
                        boxes.insert(Coordinate { x: (x * 2) as i32, y: y as i32 });
                    }
                    '@' => {
                        robot = Coordinate { x: (x * 2) as i32, y: y as i32 };
                    }
                    _ => {}
                }
            });
        });

        println!("Start");
        print_maze(&walls, &boxes, &robot);

        instructions.chars().enumerate().for_each(|(i, instr)| {
            println!("{i}");

            let direction = match instr {
                '^' => Coordinate { x: 0, y: -1 },
                '>' => Coordinate { x: 1, y: 0 },
                '<' => Coordinate { x: -1, y: 0 },
                'v' => Coordinate { x: 0, y: 1 },
                _ => panic!("Invalid instruction: {instr}")
            };

            let new_loc = &robot + &direction;

            if walls.contains(&new_loc) {
                return;
            }

            if direction.x == 0 {
                let new_loc_left = &new_loc + &Coordinate { x: -1, y: 0 };

                if boxes.contains(&new_loc) || boxes.contains(&new_loc_left) {
                    let mut to_move = VecDeque::new();
                    if boxes.contains(&new_loc) {
                        to_move.push_back(new_loc);
                    } else {
                        to_move.push_back(new_loc_left);
                    }
                    let mut to_move_total = to_move.clone();

                    while let Some(moving_box) = to_move.pop_front() {
                        let up = &moving_box + &direction;
                        let up_left = &up + &Coordinate { x: -1, y: 0 };
                        let up_right = &up + &Coordinate { x: 1, y: 0 };

                        if walls.contains(&up) || walls.contains(&up_right) {
                            return;
                        }

                        if boxes.contains(&up) && !to_move.contains(&up) {
                            to_move.push_back(up);
                            to_move_total.push_front(up);
                        }
                        if boxes.contains(&up_left) && !to_move.contains(&up_left) {
                            to_move.push_back(up_left);
                            to_move_total.push_front(up_left);
                        }
                        if boxes.contains(&up_right) && !to_move.contains(&up_right) {
                            to_move.push_back(up_right);
                            to_move_total.push_front(up_right);
                        }
                    }

                    to_move_total.iter().for_each(|moving_box| {
                        assert!(boxes.remove(moving_box));
                        assert!(boxes.insert(moving_box + &direction));
                    });
                }

                robot = new_loc;
            } else if direction.x == 1 {
                if boxes.contains(&new_loc) {
                    let mut after_box = new_loc;
                    let mut to_move = HashSet::new();

                    while boxes.contains(&after_box) {
                        to_move.insert(after_box);
                        after_box = &after_box + &(&direction + &direction);
                    }

                    if !walls.contains(&after_box) {
                        to_move.iter().for_each(|moving| {
                            assert!(boxes.remove(&moving));
                            assert!(boxes.insert(moving + &direction));
                        });

                        robot = new_loc;
                    }
                } else {
                    robot = new_loc;
                }
            } else if direction.x == -1 {
                if boxes.contains(&(&new_loc + &direction)) {
                    let mut after_box = &new_loc + &direction;
                    let mut to_move = HashSet::new();

                    while boxes.contains(&after_box) {
                        to_move.insert(after_box);
                        after_box = &after_box + &(&direction + &direction);
                    }

                    if !walls.contains(&(&after_box - &direction)) {
                        to_move.iter().for_each(|moving| {
                            assert!(boxes.remove(&moving));
                            assert!(boxes.insert(moving + &direction));
                        });

                        robot = new_loc;
                    }
                } else {
                    robot = new_loc;
                }
            }

            // println!("After {instr}");
            // print_maze(&walls, &boxes, &robot);
        });

        println!("End");
        print_maze(&walls, &boxes, &robot);

        boxes.iter().map(|Coordinate { x, y }| x + y * 100).sum()
    }
}
