use std::collections::{HashSet, VecDeque};
use itertools::Itertools;
use crate::DaySolver;

pub struct Day10;

impl DaySolver for Day10 {
    fn solve_part1(&self, input: &str) -> i32 {
        let heights = input.lines().map(|l| l.chars().map(|c| c.to_string().parse::<i32>().unwrap()).collect_vec()).collect_vec();
        let starting_points = input.lines().enumerate().flat_map(move |(y, l)| {
            l.chars().enumerate().filter_map(move |(x, c)| if c == '0' { Some((x, y))} else { None })
        }).collect_vec();

        let max_x = heights[0].len() - 1;
        let max_y = heights.len() - 1;

        let mut result = 0;

        for starting_point in starting_points {
            let mut visited = HashSet::new();
            let mut reachable_tops = HashSet::new();
            let mut queue = VecDeque::new();
            queue.push_back(starting_point);

            while let Some(current_loc@(current_x, current_y)) = queue.pop_front() {
                if visited.contains(&current_loc) {
                    continue;
                }
                visited.insert(current_loc);

                let current_value = heights[current_y][current_x];

                if current_value == 9 {
                    reachable_tops.insert(current_loc);
                    continue;
                }

                if current_x > 0
                    && !visited.contains(&(current_x - 1, current_y))
                    && heights[current_y][current_x - 1] == current_value + 1 {

                    queue.push_back((current_x - 1, current_y));
                }
                if current_x < max_x
                    && !visited.contains(&(current_x + 1, current_y))
                    && heights[current_y][current_x + 1] == current_value + 1 {

                    queue.push_back((current_x + 1, current_y));
                }
                if current_y > 0
                    && !visited.contains(&(current_x, current_y - 1))
                    && heights[current_y - 1][current_x] == current_value + 1 {

                    queue.push_back((current_x, current_y - 1));
                }
                if current_y < max_y
                    && !visited.contains(&(current_x, current_y + 1))
                    && heights[current_y + 1][current_x] == current_value + 1 {

                    queue.push_back((current_x, current_y + 1));
                }
            }

            result += reachable_tops.len() as i32;
        }

        result
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let heights = input.lines().map(|l| l.chars().map(|c| c.to_string().parse::<i32>().unwrap()).collect_vec()).collect_vec();
        let starting_points = input.lines().enumerate().flat_map(move |(y, l)| {
            l.chars().enumerate().filter_map(move |(x, c)| if c == '0' { Some((x, y))} else { None })
        }).collect_vec();

        let max_x = heights[0].len() - 1;
        let max_y = heights.len() - 1;

        let mut result = 0;

        for starting_point in starting_points {
            let mut visited = HashSet::<((usize, usize), Vec<(usize, usize)>)>::new();
            let mut reachable_tops = HashSet::<((usize, usize), Vec<(usize, usize)>)>::new();
            let mut queue = VecDeque::new();
            queue.push_back((starting_point, Vec::<(usize, usize)>::new()));

            while let Some((current_loc@(current_x, current_y), path)) = queue.pop_front() {
                if visited.contains(&(current_loc, path.clone())) {
                    continue;
                }
                let mut new_path = path.clone();
                new_path.push(current_loc);
                visited.insert((current_loc, new_path.clone()));

                let current_value = heights[current_y][current_x];

                if current_value == 9 {
                    reachable_tops.insert((current_loc, new_path.clone()));
                    continue;
                }

                if current_x > 0
                    && !visited.contains(&((current_x - 1, current_y), new_path.clone()))
                    && heights[current_y][current_x - 1] == current_value + 1 {

                    queue.push_back(((current_x - 1, current_y), new_path.clone()));
                }
                if current_x < max_x
                    && !visited.contains(&((current_x + 1, current_y), new_path.clone()))
                    && heights[current_y][current_x + 1] == current_value + 1 {

                    queue.push_back(((current_x + 1, current_y), new_path.clone()));
                }
                if current_y > 0
                    && !visited.contains(&((current_x, current_y - 1), new_path.clone()))
                    && heights[current_y - 1][current_x] == current_value + 1 {

                    queue.push_back(((current_x, current_y - 1), new_path.clone()));
                }
                if current_y < max_y
                    && !visited.contains(&((current_x, current_y + 1), new_path.clone()))
                    && heights[current_y + 1][current_x] == current_value + 1 {

                    queue.push_back(((current_x, current_y + 1), new_path.clone()));
                }
            }

            result += reachable_tops.len() as i32;
        }

        result
    }
}
