use std::collections::HashMap;
use itertools::Itertools;
use crate::DaySolver;

pub struct Day01;

impl DaySolver for Day01 {
    fn solve_part1(&self, input: &str) -> i32 {
        let (mut l, mut r) = input.lines()
            .map(|l| l.split_once(" ").unwrap())
            .map(|(l, r)|
                (l.trim().parse::<i32>().unwrap(), r.trim().parse::<i32>().unwrap()))
            .collect::<(Vec<i32>, Vec<i32>)>();

        l.sort();
        r.sort();

        l.iter()
            .zip(r)
            .map(|(l, r)| (l - r).abs())
            .sum()
    }

    fn solve_part2(&self, input: &str) -> i32 {
        let (l, r) = input.lines()
            .map(|l| l.split_once(" ").unwrap())
            .map(|(l, r)|
            (l.trim().parse::<i32>().unwrap(), r.trim().parse::<i32>().unwrap()))
            .collect::<(Vec<i32>, Vec<i32>)>();

        let count: HashMap<&i32, usize> = r.iter().counts();

        l.iter().map(|loc| *loc * *count.get(loc).unwrap_or(&0) as i32).sum()
    }
}
