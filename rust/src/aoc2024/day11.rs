use std::collections::{HashMap, VecDeque};
use itertools::Itertools;
use crate::DaySolver;

pub struct Day11;

impl DaySolver for Day11 {
    fn solve_part1(&self, input: &str) -> i32 {
        let mut rocks = input.lines().next().unwrap().split(" ").map(|r| r.parse::<i64>().unwrap()).collect_vec();

        for _ in 0..25 {
            let mut new_rocks = Vec::new();

            rocks.clone().into_iter().for_each(|rock| {
                if rock == 0 {
                    new_rocks.push(1);
                } else if rock.ilog10() % 2 == 1 {
                    let num_digit = rock.ilog10();
                    let left = rock / 10i64.pow((num_digit + 1) / 2);
                    let right = rock % 10i64.pow((num_digit + 1) / 2);

                    new_rocks.push(left);
                    new_rocks.push(right);
                } else {
                    new_rocks.push(rock * 2024);
                }
            });

            rocks = new_rocks;
            // println!("After iter {}: {:?}", i, rocks);
        }

        rocks.len() as i32
    }

    fn solve_part2_i64(&self, input: &str) -> i64 {
        let rocks = input.lines().next().unwrap().split(" ").map(|r| r.parse::<i64>().unwrap()).collect_vec();

        let mut to_calculate = VecDeque::<(i64, i32)>::new();
        let mut nums = HashMap::<(i64, i32), i64>::new(); // num, blinks, size

        for rock in rocks.iter() {
            to_calculate.push_back((rock.clone(), 75));
        }

        while let Some(entry@(rock, iters)) = to_calculate.pop_front() {
            if nums.contains_key(&entry) {
                continue;
            }

            if iters == 0 {
                nums.insert(entry, 1);
                continue;
            }

            if rock == 0 {
                let necessary = (1i64, iters - 1);
                if let Some(result) = nums.get(&necessary) {
                    nums.insert(entry, *result);
                } else {
                    to_calculate.push_front(entry);
                    to_calculate.push_front(necessary);
                }
            } else if rock.ilog10() % 2 == 1 {
                let num_digit = rock.ilog10() + 1;
                let left_rock = (rock / 10i64.pow(num_digit / 2), iters - 1);
                let right_rock = (rock % 10i64.pow(num_digit / 2), iters - 1);

                if nums.contains_key(&left_rock) && nums.contains_key(&right_rock) {
                    nums.insert(entry, nums.get(&left_rock).unwrap() + nums.get(&right_rock).unwrap());
                } else {
                    to_calculate.push_front(entry);
                    to_calculate.push_front(right_rock);
                    to_calculate.push_front(left_rock);
                }
            } else {
                let next = (rock * 2024, iters - 1);
                if let Some(result) = nums.get(&next) {
                    nums.insert(entry, *result);
                } else {
                    to_calculate.push_front(entry);
                    to_calculate.push_front(next);
                }
            }
        }

        rocks.into_iter().map(|rock| nums.get(&(rock, 75)).unwrap()).sum()
    }
}
